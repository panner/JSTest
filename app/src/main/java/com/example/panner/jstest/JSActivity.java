package com.example.panner.jstest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class JSActivity extends AppCompatActivity {
    private WebView mWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_js);
        mWebView = findViewById(R.id.web_view);
        WebSettings settings = mWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setDefaultTextEncodingName("UTF-8");
        mWebView.setWebChromeClient(new WebChromeClient());
        mWebView.setWebViewClient(new WebViewClient());
        mWebView.addJavascriptInterface(new AndroidJSMethod(), "AndroidJSMethod");
        mWebView.addJavascriptInterface(new AndroidToast(), "AndroidToast");
        mWebView.addJavascriptInterface(new AndroidMessage(), "AndroidMessage");
        mWebView.loadUrl("file:///android_asset/index_test.html");
    }

    public class AndroidToast {

        @JavascriptInterface
        public void show(String str) {
            Toast.makeText(JSActivity.this, str, Toast.LENGTH_SHORT).show();
        }

    }

    public class AndroidMessage {
        @JavascriptInterface
        public String getMsg(String str) {
            return str;
        }
    }

    public void javaCallJS() {
        mWebView.loadUrl("javascript:callFromJava('call from java')");
    }

    @Override
    public void onBackPressed() {

        javaCallJS();

    }
}
