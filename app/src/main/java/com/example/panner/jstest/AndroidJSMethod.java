package com.example.panner.jstest;

import android.util.Log;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

/**
 * Created by Panner on 2018/1/23.
 */

public class AndroidJSMethod {
    @JavascriptInterface
    public String copyString(){
        return "android";
    }
    @JavascriptInterface
    public void copyString(String str) {
        Log.e("======================",str);
        Toast.makeText(new MainActivity(), str, Toast.LENGTH_SHORT).show();
    }
}
